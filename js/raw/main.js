$ = jQuery;

$(document).on('ready', init);
$(window).on('load', loaded);
$(window).on('scroll', scrolling);

function init() {

    $('.about-slider').slick({
        arrows: false,
        dots: true
    });

    $('.materials-list').magnificPopup({
		delegate: '.materials-photo',
		type: 'image',
		tLoading: 'Загрузка изображения #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1]
		},
		image: {
			tError: '<a href="%url%">Изображение #%curr%</a> не может быть загружено.'
		}
	});

	$('.materials-video').magnificPopup({
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false
	});
}

function scrolling() {}

function loaded() {}

// events

$('.popup').click(function() {

    var target = $(this).attr('href');
    if (target == '' && target == '#') {
        return false;
    }

    $.magnificPopup.open({
        items: {
            src: target
        },
        type: 'inline'
    });

    return false;
});

$('.toggle-panel').click(function() {

    $('html, body').toggleClass('slide-panel-visible');
});
